       $.fn.serializeObject = function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        function getName(str)
        {
            return str.split(/\s+/).slice(0,2).join(" ");
        };

        /*$.fn.getName = function(str)
        {
            return str.split(/\s+/).slice(0,2).join(" ");
        }*/

        function displayTime(){
            var currentTime = new Date();
            var hours = currentTime.getHours();
            if(hours < 10){
                hours = '0' + hours;
            }

            var minutes = currentTime.getMinutes();
            if(minutes < 10){
                minutes = '0' + minutes;
            }

            var seconds = currentTime.getSeconds();
            if(seconds < 10){
                seconds = '0' + seconds;
            }

            var clockH2 = document.getElementById('time');
            clockH2.innerText = hours +  ":" + minutes + ":" + seconds;

            if(hours == '13' && minutes == '37' && seconds == '00'){
                $('#Text').val("Leet mannen!")

                sendSlackMessage();

                $("#Text").val("");
            }
        }

        function sendSlackMessage(){
                var send = $("#form").serializeObject();
                $.ajax({
                    url: "https://hooks.slack.com/services/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",
                    type: "Post",
                    data: JSON.stringify(send),
                    error: function (xhr, error) {
                        alert('Error! Status = ' + xhr.status + ' Message = ' + error);
                    },
                    success: function (data) {
                        console.log('successdata: ' + data);
                        $("#json").text("JSON:");
                        $('#result').text(JSON.stringify($('form').serializeObject()));
                        $('#result').text('sent: ' + JSON.stringify($('form').serializeObject()));
                    }
                });
        }

        $(document).ready(function () {
            setInterval(displayTime, 1000);

            $('#input').click(function () {
                //$('#result').text(JSON.stringify($('form').serializeObject()));
                sendSlackMessage();
                return false;
            });

            $('#nameButton').click(function(){
                var rand = Math.floor((Math.random() * 3) + 1);
                $('#randomNumber').text("number: " + rand);
                if(rand == 1)
                {
                    $('h3').first().text(getName($('h3').first().text()) + " is a kind person");
                    $('h3:odd').text(getName($('h3:odd').text()));
                    $('h3').last().text(getName($('h3').last().text()));

                    $('.kalle').css("color", "green");
                    $('.maria').css("color", "black");
                    $('.benny').css("color", "black");
                }
                else if(rand == 2)
                {
                    $('h3:odd').text(getName($('h3:odd').text()) + " is a kind person");
                    $('h3').first().text(getName($('h3').first().text()));
                    $('h3').last().text(getName($('h3').last().text()));

                    $('.maria').css("color", "green");
                    $('.kalle').css("color", "black");
                    $('.benny').css("color", "black");
                }
                else if(rand == 3)
                {
                    $('h3').last().text(getName($('h3').last().text()) + " is a kind person");
                    $('h3').first().text(getName($('h3').first().text()));
                    $('h3:odd').text(getName($('h3:odd').text()));

                    $('.benny').css("color", "green");
                    $('.kalle').css("color", "black");
                    $('.maria').css("color", "black");
                }
            });           
        });